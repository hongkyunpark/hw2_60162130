import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  message: string;
  names: string[] = ["obama", "trump", "clinton", "bush"];
  n: string; //입력 받아오는
  input: string;
  temp: number;
 
  constructor(public navCtrl: NavController) {

    for (var f of this.names) {
      console.log(f);
    }
  }

  keyPressed(event) {
    console.log(event);
  }

  //implement buttons
  clear(){ //empty the array
    this.names.length = 0;
    this.message = "Data was cleared";
  }

  add(key){ //add new entry at the end
    this.names.push(this.input);
    this.message = this.input + " was added";
  }

  pop(){ //remove the last enrty
    this.temp = this.names.length;
    this.n = this.names.pop();
    if (this.temp !=0)
     this.message = this.n + " was removed from the tail";
    else
     this.message = "The data is not available" //데이터가 없을 경우
  }

  shift(){ //remove the first entry
    this.temp = this.names.length;
    this.n = this.names.shift();
    if (this.temp !=0)
      this.message = this.n + " was removed from the head"
    else
     this.message = "The data is not available" //데이터가 없을 경우
  }

  sort(){ //sort the list
    this.names.sort();
    this.message = "Data sorted";
  }
}
